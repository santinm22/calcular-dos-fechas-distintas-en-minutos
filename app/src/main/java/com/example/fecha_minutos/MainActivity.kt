package com.example.fecha_minutos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.DatePicker
import android.widget.TextView
import java.util.*

lateinit var fecha_calendario:DatePicker

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        fecha_calendario= findViewById(R.id.fecha)
        val boton: Button = findViewById(R.id.btnCalcular)
        val texto: TextView =findViewById(R.id.Resultado)
        boton.setOnClickListener {
            texto.text="La fecha es ${getDate()} y hoy es ${getDateHoy()} y los minutos totales son: ${calcular_Minutos()}"
        }

    }

    fun calcular_Minutos(): Int {
        var dia_minutos2:Int? = (fecha_calendario?.dayOfMonth)
        if (dia_minutos2 != null) {
            dia_minutos2 *= 1440
        }
        var mes_minutos2:Int = (fecha_calendario!!.month+1)
        if (mes_minutos2 != null) {
            mes_minutos2 *= 43200
        }
        var anho_minutos2:Int? = fecha_calendario?.year
        if (anho_minutos2 != null) {
            anho_minutos2 *= 525600
        }
        var total_Fecha2:Int=0
        if (dia_minutos2 != null) {
            if (anho_minutos2 != null) {
                total_Fecha2 = dia_minutos2 + mes_minutos2 + anho_minutos2
            }
        }
        var minutos_dia_hoy = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        minutos_dia_hoy *= 1440
        var minutos_mes_actual = Calendar.getInstance().get(Calendar.MONTH)
        minutos_mes_actual = minutos_mes_actual+1
        minutos_mes_actual *= 43200
        var minutos_anho_actual = Calendar.getInstance().get(Calendar.YEAR)
        minutos_anho_actual *= 525600
        var total_Fecha1:Int=0
        total_Fecha1 = minutos_dia_hoy + minutos_mes_actual + minutos_anho_actual

        var minutos_Totales:Int=0
        minutos_Totales = total_Fecha1-total_Fecha2
        return minutos_Totales
    }

    fun getDate():String{
        var dia = fecha_calendario?.dayOfMonth.toString().padStart(2,'0')
        var mes = (fecha_calendario!!.month+1).toString().padStart(2,'0')
        var year = fecha_calendario?.year.toString().padStart(4,'0')
        return dia+"/"+mes+"/"+year
    }

    fun getDateHoy():String{
        var dia_hoy = Calendar.getInstance().get(Calendar.DAY_OF_MONTH).toString()
        var mes_actual = Calendar.getInstance().get(Calendar.MONTH)
        mes_actual = mes_actual+1
        var anho_actual = Calendar.getInstance().get(Calendar.YEAR).toString()
        return dia_hoy+"/"+mes_actual+"/"+anho_actual
    }
}